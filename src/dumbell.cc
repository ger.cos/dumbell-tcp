/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <fstream>
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("FirstScriptExample");

// Network topology
//
//       n0 ---+                +--- n5
//             |                |
//       n1 ---+--- n3 --- n4---+--- n6
//             |                |
//       n2 ---+                +--- n7
//
// - All links are pointToPoint with 500kb/s and 2ms
// - TCP flow form n0 to n2
// - UDP flow from n1 to n3

class MyApp : public Application 
{
public:

  MyApp ();
  virtual ~MyApp();

  void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate);

private:
  virtual void StartApplication (void);
  virtual void StopApplication (void);

  void ScheduleTx (void);
  void SendPacket (void);

  Ptr<Socket>     m_socket;
  Address         m_peer;
  uint32_t        m_packetSize;
  uint32_t        m_nPackets;
  DataRate        m_dataRate;
  EventId         m_sendEvent;
  bool            m_running;
  uint32_t        m_packetsSent;
};

MyApp::MyApp ()
  : m_socket (0), 
    m_peer (), 
    m_packetSize (0), 
    m_nPackets (0), 
    m_dataRate (0), 
    m_sendEvent (), 
    m_running (false), 
    m_packetsSent (0)
{
}

MyApp::~MyApp()
{
  m_socket = 0;
}

void
MyApp::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate)
{
  m_socket = socket;
  m_peer = address;
  m_packetSize = packetSize;
  m_nPackets = nPackets;
  m_dataRate = dataRate;
}

void
MyApp::StartApplication (void)
{
  m_running = true;
  m_packetsSent = 0;
  m_socket->Bind ();
  m_socket->Connect (m_peer);
  SendPacket ();
}

void 
MyApp::StopApplication (void)
{
  m_running = false;

  if (m_sendEvent.IsRunning ())
    {
      Simulator::Cancel (m_sendEvent);
    }

  if (m_socket)
    {
      m_socket->Close ();
    }
}

void 
MyApp::SendPacket (void)
{
  Ptr<Packet> packet = Create<Packet> (m_packetSize);
  m_socket->Send (packet);

  if (++m_packetsSent < m_nPackets)
    {
      ScheduleTx ();
    }
}

void 
MyApp::ScheduleTx (void)
{
//Velocidad, y el Tamanio
  if (m_running)
    {
      Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
      m_sendEvent = Simulator::Schedule (tNext, &MyApp::SendPacket, this);
    }
}

static void
CwndChange (uint32_t oldCwnd, uint32_t newCwnd)
{
  NS_LOG_UNCOND ("SEG: " << Simulator::Now ().GetSeconds () << "\t" << newCwnd);
}

static void
RxDrop (Ptr<const Packet> p)
{
  NS_LOG_UNCOND ("RxDrop at " << Simulator::Now ().GetSeconds ());
}



int
main (int argc, char *argv[])
{
    CommandLine cmd;
    cmd.Parse (argc, argv);

    Time::SetResolution (Time::NS);
    LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
    LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

    NodeContainer nodes;
    nodes.Create(8);

    NodeContainer n0n3 = NodeContainer (nodes.Get (0), nodes.Get (3));
    NodeContainer n1n3 = NodeContainer (nodes.Get (1), nodes.Get (3));
    NodeContainer n2n3 = NodeContainer (nodes.Get (2), nodes.Get (3));
    
    NodeContainer n3n4 = NodeContainer (nodes.Get (3), nodes.Get (4));

    NodeContainer n5n4 = NodeContainer (nodes.Get (5), nodes.Get (4));
    NodeContainer n6n4 = NodeContainer (nodes.Get (6), nodes.Get (4));
    NodeContainer n7n4 = NodeContainer (nodes.Get (7), nodes.Get (4));

    InternetStackHelper stack;
    stack.Install (nodes);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
    pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

    NetDeviceContainer d0d3 = pointToPoint.Install (n0n3);
    NetDeviceContainer d1d3 = pointToPoint.Install (n1n3);
    NetDeviceContainer d2d3 = pointToPoint.Install (n2n3);

    NetDeviceContainer d3d4 = pointToPoint.Install (n3n4);
    
    NetDeviceContainer d5d4 = pointToPoint.Install (n5n4);
    NetDeviceContainer d6d4 = pointToPoint.Install (n6n4);
    NetDeviceContainer d7d4 = pointToPoint.Install (n7n4);

    // n1---n2---n3

    Ipv4AddressHelper ipv4;
    ipv4.SetBase ("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer i0i3 = ipv4.Assign (d0d3);
    ipv4.SetBase ("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer i1i3 = ipv4.Assign (d1d3);
    ipv4.SetBase ("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer i2i3 = ipv4.Assign (d2d3);

    ipv4.SetBase ("10.1.4.0", "255.255.255.0");
    Ipv4InterfaceContainer i3i4 = ipv4.Assign (d3d4);

    ipv4.SetBase ("10.1.5.0", "255.255.255.0");
    Ipv4InterfaceContainer i5i4 = ipv4.Assign (d5d4);
    ipv4.SetBase ("10.1.6.0", "255.255.255.0");
    Ipv4InterfaceContainer i6i4 = ipv4.Assign (d6d4);
    ipv4.SetBase ("10.1.7.0", "255.255.255.0");
    Ipv4InterfaceContainer i7i4 = ipv4.Assign (d7d4);

     //Hago el ruteo magico
    Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

    /*
    //Ya tengo la topologia, ahora tengo que cargar las APPS
    UdpEchoServerHelper echoServer (9);

    ApplicationContainer serverApps = echoServer.Install (nodes.Get (5));
    serverApps.Start (Seconds (1.0));
    serverApps.Stop (Seconds (10.0));

    UdpEchoClientHelper echoClient (i5i4.GetAddress (0), 9);
    echoClient.SetAttribute ("MaxPackets", UintegerValue (1));
    echoClient.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
    echoClient.SetAttribute ("PacketSize", UintegerValue (1024));

    ApplicationContainer clientApps = echoClient.Install (nodes.Get (0));
    clientApps.Start (Seconds (2.0));
    clientApps.Stop (Seconds (10.0));
    */

    uint16_t sinkPort = 8080;
    Address sinkAddress (InetSocketAddress(i5i4.GetAddress (0), sinkPort));
    PacketSinkHelper packetSinkHelper ("ns3::TcpSocketFactory",
    InetSocketAddress (Ipv4Address::GetAny (), sinkPort));
    ApplicationContainer sinkApps = packetSinkHelper.Install (nodes.Get (5));
    sinkApps.Start (Seconds (0.0));
    sinkApps.Stop (Seconds (20.0));
/*
    uint16_t sinkPort2 = 8080;
    Address sinkAddress2 (InetSocketAddress(i6i4.GetAddress (0), sinkPort2));
    PacketSinkHelper packetSinkHelper2 ("ns3::TcpSocketFactory",
    InetSocketAddress (Ipv4Address::GetAny (), sinkPort2));
    ApplicationContainer sinkApps2 = packetSinkHelper.Install (nodes.Get (6));
    sinkApps.Start (Seconds (0.));
    sinkApps.Stop (Seconds (20.));
*/
    Ptr<Socket> ns3TcpSocket = Socket::CreateSocket (nodes.Get (0),
    TcpSocketFactory::GetTypeId ());
    ns3TcpSocket->TraceConnectWithoutContext ("CongestionWindow",
    MakeCallback (&CwndChange));

    Ptr<MyApp> app = CreateObject<MyApp> ();
    app->Setup (ns3TcpSocket, sinkAddress, 1040, 1000, DataRate ("1Mbps"));
    nodes.Get (0)->AddApplication (app);
    app->SetStartTime(Seconds(1.0));
    app->SetStopTime(Seconds(20.0));

/*
    Ptr<Socket> ns3TcpSocket2 = Socket::CreateSocket (nodes.Get (1),
    TcpSocketFactory::GetTypeId ());
    ns3TcpSocket->TraceConnectWithoutContext ("CongestionWindow",
    MakeCallback (&CwndChange));

    Ptr<MyApp> app2 = CreateObject<MyApp> ();
    app->Setup (ns3TcpSocket2, sinkAddress2, 1040, 1000, DataRate ("1Mbps"));
    nodes.Get (1)->AddApplication (app);
    app->Start (Seconds (1.));
    app->Stop (Seconds (20.));
*/
    n5n4.Get (0)->TraceConnectWithoutContext ("PhyRxDrop", MakeCallback (&RxDrop));

    Simulator::Stop (Seconds (20));
    Simulator::Run ();
    Simulator::Destroy ();
    return 0;

}
